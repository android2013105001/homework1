package com.example.zzeulki.myapplication3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class Act1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act1);

        Log.i("Act1","act1 start");
    }

    public void Onclick(View v)
    {
        if(v.getId()==R.id.act1_act2) {
            Intent i = new Intent(Act1.this, Act2.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.act1_act3)
        {
            Intent i = new Intent(Act1.this, Act3.class);
            startActivity(i);
        }
    }

    @Override
    public void onPause()
    {
        Log.i("Act1","onPause");
        super.onPause();
    }

    @Override
    public void onStart()
    {

        Log.i("Act1","onStart");
        super.onStart();
    }

    @Override
    public void onStop()
    {
        Log.i("Act1","onStop");
        super.onStop();
    }

    @Override
    public void onResume()
    {
        Log.i("Act1","onResume");
        super.onResume();
    }

    @Override
    public void onRestart()
    {
        Log.i("Act1","onRestart");
        super.onRestart();
    }

    @Override
    public void onDestroy()
    {
        Log.i("Act1","onDestroy");
        super.onDestroy();
    }

}
