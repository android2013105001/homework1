package com.example.zzeulki.myapplication3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2);

        Log.i("Act2","act2 start");
    }

    public void Onclick(View v)
    {
        if(v.getId()==R.id.act2_act1) {
            Intent i = new Intent(Act2.this, Act1.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.act2_act3)
        {
            Intent i = new Intent(Act2.this, Act3.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.act2_act4)
        {
            Intent i = new Intent(Act2.this, Act4.class);
            startActivity(i);
        }
    }

    @Override
    public void onPause()
    {
        Log.i("Act2","onPause");
        super.onPause();
    }

    @Override
    public void onStart()
    {

        Log.i("Act2","onStart");
        super.onStart();
    }

    @Override
    public void onStop()
    {
        Log.i("Act2","onStop");
        super.onStop();
    }

    @Override
    public void onResume()
    {
        Log.i("Act2","onResume");
        super.onResume();
    }

    @Override
    public void onRestart()
    {
        Log.i("Act2","onRestart");
        super.onRestart();
    }

    @Override
    public void onDestroy()
    {
        Log.i("Act2","onDestroy");
        super.onDestroy();
    }


}
