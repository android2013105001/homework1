package com.example.zzeulki.myapplication3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act4);

        Log.i("Act4","act4 start");
    }

    public void Onclick(View v)
    {
        if(v.getId()==R.id.act4_act3) {
            Intent i = new Intent(Act4.this, Act3.class);
            startActivity(i);
        }
    }

    @Override
    public void onPause()
    {
        Log.i("Act4","onPause");
        super.onPause();
    }

    @Override
    public void onStart()
    {

        Log.i("Act4","onStart");
        super.onStart();
    }

    @Override
    public void onStop()
    {
        Log.i("Act4","onStop");
        super.onStop();
    }

    @Override
    public void onResume()
    {
        Log.i("Act4","onResume");
        super.onResume();
    }

    @Override
    public void onRestart()
    {
        Log.i("Act4","onRestart");
        super.onRestart();
    }

    @Override
    public void onDestroy()
    {
        Log.i("Act4","onDestroy");
        super.onDestroy();
    }

}
