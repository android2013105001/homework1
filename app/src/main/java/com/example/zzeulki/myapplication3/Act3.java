package com.example.zzeulki.myapplication3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3);

        Log.i("Act3","act3 start");
    }

    public void Onclick(View v)
    {
        if(v.getId()==R.id.act3_act1) {
            Intent i = new Intent(Act3.this, Act1.class);
            startActivity(i);
        }
    }

    @Override
    public void onPause()
    {
        Log.i("Act3","onPause");
        super.onPause();
    }

    @Override
    public void onStart()
    {

        Log.i("Act3","onStart");
        super.onStart();
    }

    @Override
    public void onStop()
    {
        Log.i("Act3","onStop");
        super.onStop();
    }

    @Override
    public void onResume()
    {
        Log.i("Act3","onResume");
        super.onResume();
    }

    @Override
    public void onRestart()
    {
        Log.i("Act3","onRestart");
        super.onRestart();
    }

    @Override
    public void onDestroy()
    {
        Log.i("Act3","onDestroy");
        super.onDestroy();
    }

}
